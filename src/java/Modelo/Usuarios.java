package Modelo;
// Generated 12-04-2019 20:19:43 by Hibernate Tools 4.3.1



/**
 * Usuarios generated by hbm2java
 */
public class Usuarios  implements java.io.Serializable {


     private Integer idusuario;
     private Rolusuario rolusuario;
     private String nombre;
     private String appaterno;
     private String apmaterno;
     private String rut;
     private String passwd;
     private Integer rolusuario_1;

    public Usuarios() {
    }

	
    public Usuarios(Rolusuario rolusuario) {
        this.rolusuario = rolusuario;
    }
    public Usuarios(Rolusuario rolusuario, String nombre, String appaterno, String apmaterno, String rut, String passwd, Integer rolusuario_1) {
       this.rolusuario = rolusuario;
       this.nombre = nombre;
       this.appaterno = appaterno;
       this.apmaterno = apmaterno;
       this.rut = rut;
       this.passwd = passwd;
       this.rolusuario_1 = rolusuario_1;
    }
   
    public Integer getIdusuario() {
        return this.idusuario;
    }
    
    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }
    public Rolusuario getRolusuario() {
        return this.rolusuario;
    }
    
    public void setRolusuario(Rolusuario rolusuario) {
        this.rolusuario = rolusuario;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getAppaterno() {
        return this.appaterno;
    }
    
    public void setAppaterno(String appaterno) {
        this.appaterno = appaterno;
    }
    public String getApmaterno() {
        return this.apmaterno;
    }
    
    public void setApmaterno(String apmaterno) {
        this.apmaterno = apmaterno;
    }
    public String getRut() {
        return this.rut;
    }
    
    public void setRut(String rut) {
        this.rut = rut;
    }
    public String getPasswd() {
        return this.passwd;
    }
    
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
    public Integer getRolusuario_1() {
        return this.rolusuario_1;
    }
    
    public void setRolusuario_1(Integer rolusuario_1) {
        this.rolusuario_1 = rolusuario_1;
    }




}


